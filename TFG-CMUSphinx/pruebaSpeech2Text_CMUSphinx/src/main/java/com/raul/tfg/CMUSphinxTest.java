

package com.raul.tfg;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import java.io.File;
import java.io.FileInputStream;


import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import edu.cmu.sphinx.decoder.adaptation.Stats;
import edu.cmu.sphinx.decoder.adaptation.Transform;
import edu.cmu.sphinx.result.WordResult;
//import edu.cmu.sphinx.result.WordResult;


public class CMUSphinxTest { 

    public static void main(String[] args) throws Exception {
       

        if (args.length == 2){
	    	
	    		
	    	
	    		ArrayList<File> listaArchivos = getFileList(args[0],args[1]);
	    		reconocerListaAudio(listaArchivos,args[0],args[1]);
	    	      
	    }else{
	        System.out.println("Numero de argumentos incorrecto");
	    }

    }

    public static File getFileName(String arg){
    	File audio;
	  	if (arg.equals("1")){
	        audio = new File("../../TFG-Audios/dialogo1/dialogo1_16.wav");
	      }
	      else if (arg.equals("2")){
	        audio = new File("../../TFG-Audios/dialogo2/dialogo2_16.wav");
	      } else {
	        audio = new File("../../TFG-Audios/primeraPrueba/primeraPrueba_44_16.wav");
	      }
	  	  return audio;
  	  
    }
    
    public static ArrayList<File>  getFileList(String tipo,String frecuencia){
    	  //ArrayList<File> listaArchivos = new ArrayList<File>();
      	
    	  File[] files = new File("../../TFG-Audios/audiosTest/"+tipo+"/"+frecuencia).listFiles();
    	  Arrays.sort(files);
    	  ArrayList <File> listaArchivos = new ArrayList<File>(Arrays.asList(files));
    	  
    	  return listaArchivos;
      }




	private static void reconocerAudio(File audio) throws Exception {
		Configuration configuration = new Configuration();

        configuration
                .setAcousticModelPath("resource:/com/raul/tfg/cmusphinx-es-5.2/model_parameters/voxforge_es_sphinx.cd_ptm_4000");
        configuration
                .setDictionaryPath("resource:/com/raul/tfg/es.dict");
        configuration
                .setLanguageModelPath("resource:/com/raul/tfg/es-20k.lm");
        
        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);

        InputStream stream = new FileInputStream(audio);
        stream.skip(44);//eliminar datos de la cabecera (44 Bytes)

        
        recognizer.startRecognition(stream);
        SpeechResult result;
        while ((result = recognizer.getResult()) != null) {

            System.out.printf("-: %s%n", result.getHypothesis());
            /*
            // Estadisticas
            System.out.println("List of recognized words and their times:");
            for (WordResult r : result.getWords()) {
                System.out.println(r);
            }

            System.out.println("Best 3 hypothesis:");
            for (String s : result.getNbest(3))
                System.out.println(s);
			*/
        }
        recognizer.stopRecognition();
		
	}



	private static void reconocerListaAudio(ArrayList<File> listaArchivos,String tipo,String frecuencia) throws Exception {
		Configuration configuration = new Configuration();

        configuration
                .setAcousticModelPath("resource:/com/raul/tfg/cmusphinx-es-5.2/model_parameters/voxforge_es_sphinx.cd_ptm_4000");
        configuration
                .setDictionaryPath("resource:/com/raul/tfg/es.dict");
        configuration
                .setLanguageModelPath("resource:/com/raul/tfg/es-20k.lm");

        
        PrintWriter writer = new PrintWriter("../../TFG-Resultados/pruebaDialogos/pruebaCMUS_"+tipo+"_"+frecuencia+".csv", "UTF-8");
        for (File audio:listaArchivos){
	        InputStream stream = new FileInputStream(audio);
	        stream.skip(44); //eliminar datos de la cabecera (44 Bytes)
	
	        StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);
	        
    		final long start = System.nanoTime();
    		
	        recognizer.startRecognition(stream);
	        
    		
    		
	        SpeechResult result;
	        long tiempo=0;
	        double confianzaAvg=0.0;
	       
	        while ((result = recognizer.getResult()) != null) {

	        	System.out.printf("-: %s%n ", result.getHypothesis());
	            writer.printf("%s ", result.getHypothesis());
	            
	            
	            
	            // tiempo de cada palabra
	            for (WordResult r : result.getWords()) {
	            	tiempo+=r.getTimeFrame().getEnd()-r.getTimeFrame().getStart();
	            	//calcular confianza
	            	confianzaAvg=r.getConfidence();
	            }
	            
	
	            
				
	            
	        }
	        recognizer.stopRecognition();
	        final long end = System.nanoTime();
    		double totalTiempo=((end - start) / 1000000.0);

	        
	        writer.printf(",");
	        writer.printf("%s,", totalTiempo);
	        writer.printf("%s%n", confianzaAvg);

	        
	        

        }
        writer.close();
	}



	private static void stats(StreamSpeechRecognizer recognizer, File audio) throws Exception{

        // Live adaptation to speaker with speaker profiles
        SpeechResult result;

        InputStream stream = new FileInputStream(audio);
        stream.skip(44);

        // Stats class is used to collect speaker-specific data
        Stats stats = recognizer.createStats(1);
        recognizer.startRecognition(stream);
        while ((result = recognizer.getResult()) != null) {
            stats.collect(result);
        }
        recognizer.stopRecognition();

        // Transform represents the speech profile
        Transform transform = stats.createTransform();
        recognizer.setTransform(transform);

        // Decode again with updated transform
        
        stream = new FileInputStream(audio);

        stream.skip(44);
        recognizer.startRecognition(stream);
        while ((result = recognizer.getResult()) != null) {
            System.out.format("Hypothesis: %s\n", result.getHypothesis());
        }
        recognizer.stopRecognition();

    }
	
	
	
	
	
	
	
}
