package com.raul.tfg;

import java.util.List;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechResults;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechTimestamp;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.Transcript;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechAlternative;

/**
 * Hello world!
 *
 */
public class WatsonTest 
{
    public static void main( String[] args ) throws Exception
    {

		if (args.length == 2){
	    	
	    	
	    	ArrayList<File> listaArchivos = getFileList(args[0],args[1]);
	    	reconocerListaAudio(listaArchivos,args[0],args[1]);
	    	    
	    }else{
	        System.out.println("Numero de argumentos incorrecto");
	    }

    }

    public static File getFileName(String arg){
    	File audio;
	  	if (arg.equals("1")){
	        audio = new File("../../TFG-Audios/dialogo1/dialogo1_16.wav");
	      }
	      else if (arg.equals("2")){
	        audio = new File("../../TFG-Audios/dialogo2/dialogo2_16.wav");
	      } else {
	        audio = new File("../../TFG-Audios/primeraPrueba/primeraPrueba_44_16.wav");
	      }
	  	  return audio;
  	  
    }
    
    public static ArrayList<File>  getFileList(String tipo,String frecuencia){
  	  //ArrayList<File> listaArchivos = new ArrayList<File>();
    	
  	  File[] files = new File("../../TFG-Audios/audiosTest/"+tipo+"/"+frecuencia).listFiles();
  	  Arrays.sort(files);
  	  ArrayList <File> listaArchivos = new ArrayList<File>(Arrays.asList(files));
  	  
  	  return listaArchivos;
    }
    
    public static void reconocerAudio(File audio) throws Exception{
    	
    	SpeechToText service = new SpeechToText();
		service.setUsernameAndPassword("5d549a4e-48b1-41ba-8e17-814080fb5bd9", "QnVb8zzEV0ye");
		
    	RecognizeOptions options = new RecognizeOptions.Builder()
    	          .model("es-ES_BroadbandModel")
    	          .contentType("audio/wav")
    	          //.interimResults(true)
    	          .inactivityTimeout(10)
    	          .maxAlternatives(1)
    	          //.wordConfidence(true)
    	           .timestamps(true)
    	          .build();
    	          
    			SpeechResults transcript = service.recognize(audio,options).execute();
    			System.out.println(transcript.getResults().get(0).getAlternatives().get(0).getTranscript());
        		//System.out.println(transcript); //toda la informacion
    }
    
    
    public static void reconocerListaAudio(ArrayList<File> listaArchivos,String tipo,String frecuencia) throws Exception{
    	SpeechToText service = new SpeechToText();
		service.setUsernameAndPassword("5d549a4e-48b1-41ba-8e17-814080fb5bd9", "QnVb8zzEV0ye");
		RecognizeOptions options = null;
		if(frecuencia.equals("16")){
			options = new RecognizeOptions.Builder()
    	          .model("es-ES_BroadbandModel")
    	          .contentType("audio/wav")
    	          //.interimResults(true)
    	          .inactivityTimeout(10)
    	          .maxAlternatives(1)
    	          //.wordConfidence(true)
    	           .timestamps(true)
    	          .build();
		}else if(frecuencia.equals("8")){
			options = new RecognizeOptions.Builder()
	    	          .model("es-ES_NarrowbandModel")
	    	          .contentType("audio/wav")
	    	          //.interimResults(true)
	    	          .inactivityTimeout(10)
	    	          .maxAlternatives(1)
	    	          //.wordConfidence(true)
	    	           .timestamps(true)
	    	          .build();
		}
    	PrintWriter writer = new PrintWriter("../../TFG-Resultados/pruebaDialogos/pruebaWatson_"+tipo+"_"+frecuencia+".csv", "UTF-8");
    	for (File audio:listaArchivos){
    		
    		final long start = System.nanoTime();
    		
    		SpeechResults transcript = service.recognize(audio,options).execute();
    		List<Transcript> resultados=transcript.getResults();
    		
    		final long end = System.nanoTime();
    		
    		double totalTiempo=((end - start) / 1000000.0);
    		
    		System.out.printf("-: %s%n",transcript);
			
	

    		if (!resultados.isEmpty()){
    			if (!resultados.get(0).getAlternatives().isEmpty()){
    				SpeechAlternative alternative = resultados.get(0).getAlternatives().get(0);
	    			writer.printf("%s,",alternative.getTranscript());
	    			writer.printf("%s,",totalTiempo);
	    			writer.printf("%s%n",alternative.getConfidence());
    			}
    			else{
    				
    			}

    		}else{
    			writer.printf("%s,"," ");
    			writer.printf("%s,",totalTiempo);
    			writer.printf("%s%n","0.0");
    		}
    		//System.out.println(transcript); //toda la informacion
    		
    	}
    	writer.close();
    }
    
    public static String getTimeStamp(List<SpeechTimestamp> ts){
    	
    	Double tiempoTotal=0.0;
    	for (SpeechTimestamp speech:ts){
    		tiempoTotal+=speech.getEndTime()-speech.getStartTime();
    	}
    	
    	return tiempoTotal.toString();
    	
    	
    }
}
