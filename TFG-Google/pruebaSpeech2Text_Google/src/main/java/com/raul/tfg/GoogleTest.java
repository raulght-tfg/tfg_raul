

package com.raul.tfg;

import com.google.cloud.speech.v1.RecognitionAudio;
import com.google.cloud.speech.v1.RecognitionConfig;
import com.google.cloud.speech.v1.RecognizeResponse;
import com.google.cloud.speech.v1.SpeechClient;
import com.google.cloud.speech.v1.SpeechRecognitionAlternative;
import com.google.cloud.speech.v1.SpeechRecognitionResult;
import com.google.protobuf.ByteString;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GoogleTest {
  public static void main(String... args) throws Exception {
    
    
    
    // Modo de reconocimiento
    if (args.length == 2){
    	
    		ArrayList<String> listaArchivos = getFileList(args[0],args[1]);
    		reconocerListaAudio(listaArchivos,args[0],args[1]);
    	
      
      
    }else{

        System.out.println("Numero de argumentos incorrecto");
    }
    
   
  }
  public static String getFileName(String arg){
	  String fileName="";
	  if (arg.equals("1")){
	        fileName = "../../TFG-Audios/dialogo1/dialogo1_16.wav";
	      }
	      else if (arg.equals("2")){
	        fileName = "../../TFG-Audios/dialogo2/dialogo2_16.wav";
	      } else {
	        fileName = "../../TFG-Audios/primeraPrueba/primeraPrueba_44_16.wav";
	      }
	  return fileName;
	  
  }
  
  public static ArrayList<String>  getFileList(String tipo,String frecuencia){
  	  //ArrayList<File> listaArchivos = new ArrayList<File>();
    	
  	  File[] files = new File("../../TFG-Audios/audiosTest/"+tipo+"/"+frecuencia).listFiles();
  	  Arrays.sort(files);
  	  ArrayList <File> listaArchivos = new ArrayList<File>(Arrays.asList(files));
  	  
  	  ArrayList<String> nombreArchivos = new ArrayList<String>();
  	  for (File f:listaArchivos){
  		  nombreArchivos.add(f.getPath());
  	  }
  	  
  	  return nombreArchivos;
    }
 
  
  public static void reconocerAudio(String fileName) throws Exception{
	    SpeechClient speech = SpeechClient.create();
	    Path path = Paths.get(fileName);
	    byte[] data = Files.readAllBytes(path);
	    ByteString audioBytes = ByteString.copyFrom(data);

	    RecognitionConfig config = RecognitionConfig.newBuilder()
	        
	        .setLanguageCode("es-ES")
	        
	        .build();
	    RecognitionAudio audio = RecognitionAudio.newBuilder()
	        .setContent(audioBytes)
	        
	        .build();

	    RecognizeResponse response = speech.recognize(config, audio);
	    List<SpeechRecognitionResult> results = response.getResultsList();

	    for (SpeechRecognitionResult result: results) {
	      List<SpeechRecognitionAlternative> alternatives = result.getAlternativesList();
	      for (SpeechRecognitionAlternative alternative: alternatives) {
	        System.out.printf("Transcription: %s%n", alternative.getTranscript());
	      }
	    }
	    speech.close();

	  
  }
  public static void reconocerListaAudio(ArrayList<String> listaArchivos,String tipo,String frecuencia) throws Exception{
	  SpeechClient speech = SpeechClient.create();
	  Path path;
	  byte[]data;
	  ArrayList <ByteString> listaBytes= new ArrayList<ByteString>();
	  for (String temp:listaArchivos){
		  path = Paths.get(temp);
		  data = Files.readAllBytes(path);
		  listaBytes.add(ByteString.copyFrom(data));
	  }
	  RecognitionConfig config = RecognitionConfig.newBuilder()
		        
		        .setLanguageCode("es-ES")
		        
		        .build();
	  
	  ArrayList <RecognitionAudio> audioList= new ArrayList<RecognitionAudio>();
      PrintWriter writer = new PrintWriter("../../TFG-Resultados/pruebaDialogos/pruebaGoogle_"+tipo+"_"+frecuencia+".csv", "UTF-8");
	  for (ByteString audioBytes:listaBytes){
		  RecognitionAudio audio = RecognitionAudio.newBuilder()
			        .setContent(audioBytes)
			        
			        .build();
		  audioList.add(audio);
  		final long start = System.nanoTime();
		  RecognizeResponse response = speech.recognize(config, audio);
		  List<SpeechRecognitionResult> results = response.getResultsList();
  		final long end = System.nanoTime();
		double totalTiempo=((end - start) / 1000000.0);

			if (results.isEmpty()){
				writer.printf("%s,"," ");
    			writer.printf("%s,",totalTiempo);
    			writer.printf("%s%n","0.0");
			}
		    for (SpeechRecognitionResult result: results) {
		      List<SpeechRecognitionAlternative> alternatives = result.getAlternativesList();
		      for (SpeechRecognitionAlternative alternative: alternatives) {
		        System.out.printf("-: %s%n", alternative.getTranscript());
		        writer.printf("%s,", alternative.getTranscript());
		        writer.printf("%s,", totalTiempo);
		        writer.printf("%s\n", alternative.getConfidence());

		        
		      }
		    }
	  }
	   	writer.close();
	    speech.close();
		    
	  
  }
  

  
}

