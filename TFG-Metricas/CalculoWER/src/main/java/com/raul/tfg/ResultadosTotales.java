package com.raul.tfg;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import au.com.bytecode.opencsv.CSVReader;

public class ResultadosTotales {
	public static void main(String[] args) throws IOException {
		  File[] files = new File("../../TFG-Resultados/resultadosWER").listFiles();
	  	  Arrays.sort(files);
	  	  ArrayList <File> listaArchivos = new ArrayList<File>(Arrays.asList(files));
	  	  int numFiles=listaArchivos.size();
	  	  int sumaGoogle=0,sumaWatson=0,sumaCMUS=0,sumaWitAI=0;
	  	  
	  	  double timeTotalWatson = 0.0;
	  	  double timeTotalGoogle = 0.0;
	  	  double timeTotalCMUS = 0.0;
	  	  double timeTotalWitAI = 0.0;
	  	  
	  	  ArrayList<List<Double>> datosWerExt = new ArrayList<List<Double>>();
	  	  //inicializar matriz
	  	  for (int i =0; i<4;i++){
	  		datosWerExt.add(new ArrayList<>(Arrays.asList(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)));
	  	  }
	  	 
	  	  
	  	for (File f:listaArchivos){
	  		if (f.getName().startsWith("prueba")){
				CSVReader readerTotal = new CSVReader(new FileReader(f), ',', '"', 0);
				List<String[]> lstTotal = readerTotal.readAll();
				String [] ultimafila=lstTotal.get(lstTotal.size()-1);
				sumaWatson+=Integer.parseInt(ultimafila[5]);
				sumaGoogle+=Integer.parseInt(ultimafila[6]);
				sumaCMUS+=Integer.parseInt(ultimafila[7]);
				sumaWitAI+=Integer.parseInt(ultimafila[8]);
				timeTotalWatson+=Double.parseDouble(ultimafila[9]);
				timeTotalGoogle+=Double.parseDouble(ultimafila[10]);
				timeTotalCMUS+=Double.parseDouble(ultimafila[11]);
				timeTotalWitAI+=Double.parseDouble(ultimafila[12]);
	  		}else{
	  			CSVReader readerTotal = new CSVReader(new FileReader(f), ',', '"', 0);
				List<String[]> lstTotal = readerTotal.readAll();
				for (int i=1;i<=4;i++){//leer lineas de los reconocedores menos la primera
					
					int j=i-1;//indice de la matriz
					String [] fila=lstTotal.get(i);
					for(int filaIndex = 1; filaIndex<fila.length;filaIndex++){
						datosWerExt.get(j).set(filaIndex-1,datosWerExt.get(j).get(filaIndex-1)+Double.parseDouble(fila[filaIndex]));
					}
				}
	  			
	  		}


	  	}
	  	numFiles=numFiles/2;//Hay dos tipos de archivos
	  	PrintWriter writer = new PrintWriter(
				"../../TFG-Resultados/resultadoTotal.csv", "UTF-8");
	  	writer.println(",Watson,Google,Wit.AI,CMUS");
	  	writer.printf("media WER,%s,%s,%s,%s%n",sumaWatson/numFiles,sumaGoogle/numFiles,sumaWitAI/numFiles,sumaCMUS/numFiles);

	  	writer.printf(Locale.US, "media tiempo (s),%.3f,%.3f,%.3f,%.3f%n",(timeTotalWatson/numFiles)/1000.0,
				(timeTotalGoogle/numFiles)/1000.0,(timeTotalWitAI/numFiles)/1000.0,(timeTotalCMUS/numFiles)/1000.0);

		writer.printf(Locale.US,"media Correccion,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(0)/(double)numFiles),
				(datosWerExt.get(1).get(0)/(double)numFiles),(datosWerExt.get(2).get(0)/(double)numFiles),
		(datosWerExt.get(3).get(0)/(double)numFiles));
		writer.printf(Locale.US,"media Sustitucion,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(1)/(double)numFiles),
				(datosWerExt.get(1).get(1)/(double)numFiles),(datosWerExt.get(2).get(1)/(double)numFiles),
		(datosWerExt.get(3).get(1)/(double)numFiles));
		writer.printf(Locale.US,"media Insercion,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(2)/(double)numFiles),
				(datosWerExt.get(1).get(2)/(double)numFiles),(datosWerExt.get(2).get(2)/(double)numFiles),
		(datosWerExt.get(3).get(2)/(double)numFiles));
		writer.printf(Locale.US,"media Supresion,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(3)/(double)numFiles),
				(datosWerExt.get(1).get(3)/(double)numFiles),(datosWerExt.get(2).get(3)/(double)numFiles),
		(datosWerExt.get(3).get(3)/(double)numFiles));
		writer.printf(Locale.US,"media Palabras erroneas,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(4)/(double)numFiles),
				(datosWerExt.get(1).get(4)/(double)numFiles),(datosWerExt.get(2).get(4)/(double)numFiles),
		(datosWerExt.get(3).get(4)/(double)numFiles));
		writer.printf(Locale.US,"media Frases erroneas,%.3f,%.3f,%.3f,%.3f%n",(datosWerExt.get(0).get(5)/(double)numFiles),
				(datosWerExt.get(1).get(5)/(double)numFiles),(datosWerExt.get(2).get(5)/(double)numFiles),
		(datosWerExt.get(3).get(5)/(double)numFiles));

		writer.close();
		
	
		
		
	}

}
