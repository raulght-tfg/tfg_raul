
package com.raul.tfg;


import java.io.FileReader;


import java.io.IOException;
import java.io.PrintWriter;

import au.com.bytecode.opencsv.CSVReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import java.util.stream.Stream;
import com.pwnetics.metric.WordSequenceAligner;
import com.pwnetics.metric.WordSequenceAligner.Alignment;
import com.pwnetics.metric.WordSequenceAligner.SummaryStatistics;

public class CalculoWER {

	public static void main(String[] args) throws IOException {
		// tipo de prueba
		Path transcripcion = null;
		String resultWatson = null, resultGoogle = null, resultCMUS = null, resultWitAI = null;
		if (args.length == 2) {
			transcripcion = Paths.get("../../TFG-Transcripciones/dialogo" + args[0] + ".txt");
			resultWatson = "../../TFG-Resultados/pruebaDialogos/pruebaWatson_" + args[0] + "_" + args[1] + ".csv";
			resultGoogle = "../../TFG-Resultados/pruebaDialogos/pruebaGoogle_" + args[0] + "_" + args[1] + ".csv";
			resultCMUS = "../../TFG-Resultados/pruebaDialogos/pruebaCMUS_" + args[0] + "_" + args[1] + ".csv";
			resultWitAI = "../../TFG-Resultados/pruebaDialogos/pruebaWitAI_" + args[0] + "_" + args[1] + ".csv";
		}

		List<String> lstTrans = new ArrayList<String>();

		CSVReader readerWatson = new CSVReader(new FileReader(resultWatson), ',', '"', 0);
		CSVReader readerGoogle = new CSVReader(new FileReader(resultGoogle), ',', '"', 0);
		CSVReader readerCMUS = new CSVReader(new FileReader(resultCMUS), ',', '"', 0);
		CSVReader readerWitAI = new CSVReader(new FileReader(resultWitAI), ',', '"', 0);

		List<String[]> lstWatson = readerWatson.readAll();
		List<String[]> lstGoogle = readerGoogle.readAll();
		List<String[]> lstCMUS = readerCMUS.readAll();
		List<String[]> lstWitAI = readerWitAI.readAll();

		Stream<String> stTrans = Files.lines(transcripcion);

		stTrans.forEach((s) -> lstTrans.add(s));

		
		ArrayList<Integer> werWatson = new ArrayList<Integer>(), werGoogle = new ArrayList<Integer>(),
				werCMUS = new ArrayList<Integer>(), werWitAI = new ArrayList<Integer>();
		int werTotalWatson = 0, werTotalGoogle = 0, werTotalCMUS = 0, werTotalWitAI = 0;
		double timeTotalWatson = 0;
		double timeTotalGoogle = 0.0;
		double timeTotalCMUS = 0.0;
		double timeTotalWitAI = 0.0;
		
		//WER extendido
		WordSequenceAligner werEval = new WordSequenceAligner();
		
		List<Alignment> werExtWatson = new ArrayList<WordSequenceAligner.Alignment>();
		List<Alignment> werExtGoogle = new ArrayList<WordSequenceAligner.Alignment>();
		List<Alignment> werExtCMUS = new ArrayList<WordSequenceAligner.Alignment>();
		List<Alignment> werExtWitAI = new ArrayList<WordSequenceAligner.Alignment>();




		for (int i = 0; i < lstTrans.size(); i++) {
			System.out.println("Transcripcion frase nº " + (i + 1) + ": " + lstTrans.get(i));
			System.out.println();

			System.out.println("	Resultado Watson: " + lstWatson.get(i)[0]);
			ArrayList<String> auxTrans = getWords(lstTrans.get(i));
			ArrayList<String> auxWatson = getWords(lstWatson.get(i)[0]);
			werWatson.add(getWER(auxTrans,auxWatson));
			
			werExtWatson.add(werEval.align(auxTrans.toArray(new String[auxTrans.size()]),auxWatson.toArray(new String[auxWatson.size()])));
			werTotalWatson += werWatson.get(i);
			timeTotalWatson += Double.parseDouble(lstWatson.get(i)[1]);

			System.out.println("	WER Watson: " + werWatson.get(i));

			System.out.println("	Resultado Google: " + lstGoogle.get(i)[0]);
			ArrayList<String> auxGoogle = getWords(lstGoogle.get(i)[0]);

			werGoogle.add(getWER(auxTrans, auxGoogle));
			werExtGoogle.add(werEval.align(auxTrans.toArray(new String[auxTrans.size()]),
					auxGoogle.toArray(new String[auxGoogle.size()])));
			werTotalGoogle += werGoogle.get(i);
			timeTotalGoogle += Double.parseDouble(lstGoogle.get(i)[1]);

			System.out.println("	WER Google: " + werGoogle.get(i));

			System.out.println("	Resultado WitAi: " + lstWitAI.get(i)[0]);
			ArrayList<String> auxWitAi = getWords(lstWitAI.get(i)[0]);

			werWitAI.add(getWER(auxTrans, auxWitAi));
			werExtWitAI.add(werEval.align(auxTrans.toArray(new String[auxTrans.size()]),
					auxWitAi.toArray(new String[auxWitAi.size()])));
			werTotalWitAI += werWitAI.get(i);
			timeTotalWitAI += Double.parseDouble(lstWitAI.get(i)[1]);

			System.out.println("	WER Wit.Ai: " + werWitAI.get(i));

			System.out.println("	Resultado CMUSphinx: " + lstCMUS.get(i)[0]);
			ArrayList<String> auxCMUS = getWords(lstCMUS.get(i)[0]);
			werCMUS.add(getWER(auxTrans, auxCMUS));
			werTotalCMUS += werCMUS.get(i);
			werExtCMUS.add(werEval.align(auxTrans.toArray(new String[auxTrans.size()]),
					auxCMUS.toArray(new String[auxCMUS.size()])));
			timeTotalCMUS += Double.parseDouble(lstCMUS.get(i)[1]);

			System.out.println("	WER CMUSphinx: " + werCMUS.get(i));

			System.out.println();

		}

		System.out.println("-----------------------------");
		System.out.println("WER TOTAL Watson = " + werTotalWatson);
		System.out.println("WER TOTAL Google = " + werTotalGoogle);
		System.out.println("WER TOTAL CMUS = " + werTotalCMUS);
		System.out.println("WER TOTAL Wit.AI = " + werTotalWitAI);

		PrintWriter writer = new PrintWriter(
				"../../TFG-Resultados/resultadosWER/prueba_" + args[0] + "_" + args[1] + ".csv", "UTF-8");

		writer.println("Transcripcion,Watson Trans.,Google Trans.,CMUS Trans.,WitAI Trans.,"
				+ "Watson WER,Google WER,CMUS WER,WitAI WER," + "Watson Time,Google Time,CMUS Time,WitAI Time,"
				+ "Watson Conf,Google Conf,CMUS Conf");

		for (int i = 0; i < lstTrans.size(); i++) {
			writer.printf("%s,", lstTrans.get(i));
			writer.printf("%s,", lstWatson.get(i)[0]);
			writer.printf("%s,", lstGoogle.get(i)[0]);
			writer.printf("%s,", lstCMUS.get(i)[0]);
			writer.printf("%s,", lstWitAI.get(i)[0]);
			writer.printf("%s,", werWatson.get(i));
			writer.printf("%s,", werGoogle.get(i));
			writer.printf("%s,", werCMUS.get(i));
			writer.printf("%s,", werWitAI.get(i));
			writer.printf(Locale.US, "%.3f,", Double.parseDouble(lstWatson.get(i)[1]));
			writer.printf(Locale.US, "%.3f,", Double.parseDouble(lstGoogle.get(i)[1]));
			writer.printf(Locale.US, "%.3f,", Double.parseDouble(lstCMUS.get(i)[1]));
			writer.printf(Locale.US, "%.3f,", Double.parseDouble(lstWitAI.get(i)[1]));
			writer.printf("%s,", lstWatson.get(i)[2]);
			writer.printf("%s,", lstGoogle.get(i)[2]);
			writer.printf("%s%n", lstCMUS.get(i)[2]);

		}
		writer.print(
				" , , , , ," + werTotalWatson + "," + werTotalGoogle + "," + werTotalCMUS + "," + werTotalWitAI + ",");
		writer.printf(Locale.US, "%.3f,%.3f,%.3f,%.3f, , , ,", timeTotalWatson, timeTotalGoogle, timeTotalCMUS,
				timeTotalWitAI);
		writer.close();
		

		PrintWriter writerExt = new PrintWriter(
				"../../TFG-Resultados/resultadosWER/werExt_" + args[0] + "_" + args[1] + ".csv", "UTF-8");
		ArrayList<SummaryStatistics> ssList= new ArrayList<SummaryStatistics>();
		ssList.add(werEval.new SummaryStatistics(werExtWatson));
		ssList.add(werEval.new SummaryStatistics(werExtGoogle));
		ssList.add(werEval.new SummaryStatistics(werExtWitAI));
		ssList.add(werEval.new SummaryStatistics(werExtCMUS));
		
		
		
		writerExt.println("Tipo,% Corrección,% Sustituciones,% Inserciones,% Supresiones,"
				+ "% WER (palabras erroneas),% SER (frases erroneas)");
		writerExt.printf(Locale.US,"Watson,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f%n",ssList.get(0).getCorrectRate(),ssList.get(0).getSubstitutionRate(),  ssList.get(0).getDeletionRate(), 
				ssList.get(0).getInsertionRate(),  ssList.get(0).getWordErrorRate(),  ssList.get(0).getSentenceErrorRate());
		writerExt.printf(Locale.US,"Google,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f%n",ssList.get(1).getCorrectRate(),ssList.get(1).getSubstitutionRate(),  ssList.get(1).getDeletionRate(), 
				ssList.get(1).getInsertionRate(),  ssList.get(1).getWordErrorRate(),  ssList.get(1).getSentenceErrorRate());
		writerExt.printf(Locale.US,"Wit.AI,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f%n",ssList.get(2).getCorrectRate(),ssList.get(2).getSubstitutionRate(),  ssList.get(2).getDeletionRate(), 
				ssList.get(2).getInsertionRate(),  ssList.get(2).getWordErrorRate(),  ssList.get(2).getSentenceErrorRate());
		writerExt.printf(Locale.US,"CMU Sphinx,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f%n",ssList.get(3).getCorrectRate(),ssList.get(3).getSubstitutionRate(),  ssList.get(3).getDeletionRate(), 
				ssList.get(3).getInsertionRate(),  ssList.get(3).getWordErrorRate(),  ssList.get(3).getSentenceErrorRate());
		writerExt.close();

	}

	public static ArrayList<String> getWords(String text) {
		text = text.replace(":- ", "").replace("?", "").replace("¿", "").replace("/", "").replace(",", "");
		ArrayList<String> words = new ArrayList<String>();
		BreakIterator breakIterator = BreakIterator.getWordInstance();
		breakIterator.setText(text);
		int lastIndex = breakIterator.first();
		while (BreakIterator.DONE != lastIndex) {
			int firstIndex = lastIndex;
			lastIndex = breakIterator.next();
			if (lastIndex != BreakIterator.DONE && Character.isLetterOrDigit(text.charAt(firstIndex))) {
				words.add(text.substring(firstIndex, lastIndex));
			}
		}

		return words;
	}

	public static int getWER(ArrayList<String> referencia, ArrayList<String> hipotesis) {
		// inicializacion
		int[][] wer = new int[referencia.size() + 1][hipotesis.size() + 1];

		for (int i = 0; i < (referencia.size() + 1); i++) {
			for (int j = 0; j < (hipotesis.size() + 1); j++) {
				if (i == 0)
					wer[0][j] = j;
				else if (j == 0)
					wer[i][0] = i;
			}
		}
		// calculo coste
		for (int i = 1; i < (referencia.size() + 1); i++) {
			for (int j = 1; j < (hipotesis.size() + 1); j++) {
				if (referencia.get(i - 1).equalsIgnoreCase(hipotesis.get(j - 1))
						|| compararNum(referencia.get(i - 1), hipotesis.get(j - 1))
						|| compararSimbolo(referencia.get(i - 1), hipotesis.get(j - 1))) {
					wer[i][j] = wer[i - 1][j - 1];
				} else {
					int sustitucion = wer[i - 1][j - 1] + 1;
					int insercion = wer[i][j - 1] + 1;
					int borrado = wer[i - 1][j] + 1;
					Set<Integer> minimos = new HashSet<Integer>();
					minimos.add(sustitucion);
					minimos.add(insercion);
					minimos.add(borrado);
					wer[i][j] = Collections.min(minimos);

				}

			}
		}

		return wer[referencia.size()][hipotesis.size()];
	}

	public static boolean compararNum(String ref, String hip) {
		n2t num2text = new n2t();
		try {
			if (ref.equalsIgnoreCase(num2text.convertirLetras((Integer.parseInt(hip))))) {
				return true;
			} else {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;

		}

	}

	public static boolean compararSimbolo(String ref, String hip) {
		if ((ref.equals("euros") || ref.equals("euro")) && hip.equals("€")) {
			return true;
		} else {
			return false;
		}
	}
}