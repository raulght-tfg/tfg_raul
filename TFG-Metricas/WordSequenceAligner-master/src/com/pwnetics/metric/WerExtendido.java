package com.pwnetics.metric;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.pwnetics.metric.WordSequenceAligner.SummaryStatistics;

public class WerExtendido {

	public static void main(String[] args) {
		WordSequenceAligner werEval = new WordSequenceAligner();
		
		alignments.add(werEval.align("the quick brown cow jumped over the moon".split(" "), "quick brown cows jumped way over the moon dude".split(" ")));
		alignments.add(werEval.align("the quick brown cow jumped over the moon".split(" "), "quick brown over the moon dude".split(" ")));
		alignments.add(werEval.align("0 1 1 0 0 1 0 0 0 1 1 1 0 0 0 0 1 1 1 0 1 1 0 1 0 1 0 1 1 1 0 0 0 0 1 1 1 0 0 1 0 1 1 0 1 0 1 1 0 1 0 0 0 1 0 1 0 0 1 0 1 0 1 1 1 0 1 1 0 0 1 1 1 0 0 1 1 1 0 0 1 0 1 1 1 1 1 0 1 0 1 0 0 1 0 0 1 1 0 1".split(" "), "1 0 1 1 0 1 1 1 1 0 0 0 0 0 1 0 0 0 1 1 0 0 1 1 1 0 0 0 0 0 1 0 0 1 0 1 1 0 0 1 1 0 0 1 1 1 1 0 0 0 1 0 1 1 0 0 1 1 1 0 1 0 0 0 1 0 0 1 1 0 0 1 0 1 1 1 1 1 0 0 1 0 0 0 1 1 1 0 1 1 1 0 1 0 1 1 1 0 0 1".split(" ")));
		
		SummaryStatistics ss = werEval.new SummaryStatistics(alignments);
		assertTrue(ss.getNumSentences() == 3);
		assertTrue(ss.getNumReferenceWords() == 116);
		assertTrue(ss.getNumHypothesisWords() == 115);
		assertEquals(0.776, ss.getCorrectRate(), 0.001);
		assertEquals(0.103, ss.getSubstitutionRate(), 0.001);
		assertEquals(0.121, ss.getDeletionRate(), 0.001);
		assertEquals(0.112, ss.getInsertionRate(), 0.001);
		assertEquals(0.336, ss.getWordErrorRate(), 0.001);
		assertEquals(1.000, ss.getSentenceErrorRate(), 0.001);
	}

}
