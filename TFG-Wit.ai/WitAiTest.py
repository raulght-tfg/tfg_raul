import requests
import json
import sys
import os
import time

from Recorder import record_audio, read_audio

API_ENDPOINT = 'https://api.wit.ai/speech'

wit_access_token = 'PTF2FUSN32YWL6JDMCL34LPNJOKCF24D'

def RecognizeSpeech(AUDIO_FILENAME, num_seconds = 5):
    
    audio = read_audio(AUDIO_FILENAME)
    
    headers = {'authorization': 'Bearer ' + wit_access_token,
               'Content-Type': 'audio/wav'}
    start=(time.time())*1000.0
    resp = requests.post(API_ENDPOINT, headers = headers,
                         data = audio)
    data = json.loads(resp.content)
    end=(time.time())*1000.0
    global latencia
    latencia=end-start
    print latencia
    try:
        text = data['_text']
    except KeyError, e:
        text=""
    
    return text

def redondeo(value):
    return "%.3f" % value
if __name__ == "__main__":
    
	
    from os.path import isfile, join
    
    tipo=sys.argv[1]
    frecuencia=sys.argv[2]
    pathAudios=os.path.realpath('../TFG-Audios/audiosTest/'+tipo+'/'+frecuencia)
    filesAudio = [f for f in os.listdir(pathAudios) if isfile(join(pathAudios, f))]
    filesAudio.sort()
    file = open(os.path.realpath("../TFG-Resultados/pruebaDialogos/pruebaWitAI_"+tipo+"_"+frecuencia+".csv"),'w') 

    for fileA in filesAudio:
        text =  RecognizeSpeech(os.path.realpath('../TFG-Audios/audiosTest/'+tipo+'/'+frecuencia+'/'+fileA), 4)
 
        file.write("{},".format(text.encode('utf-8')))
        file.write(str(redondeo(latencia))) 
        file.write(",\n") 

        
         
        print(":- {}".format(text.encode('utf-8')))
    file.close() 

