# encoding=utf8 



from __future__ import print_function
from sys import byteorder
from array import array
from struct import pack

from copy import deepcopy



from dotenv import load_dotenv

import json

from os.path import join, dirname

from watson_developer_cloud import SpeechToTextV1

from watson_developer_cloud import ConversationV1

from watson_developer_cloud import TextToSpeechV1

from watson_developer_cloud import ToneAnalyzerV3

from watson_developer_cloud import LanguageTranslatorV2

from watson_developer_cloud import NaturalLanguageUnderstandingV1

from watson_developer_cloud import DiscoveryV1

from watson_developer_cloud.natural_language_understanding_v1 import Features, EntitiesOptions, SentimentOptions


from pysndfx import AudioEffectsChain 

import pyaudio
import audioop
import wave
import math
import time
import os
import csv

from termcolor import colored

import sys  

reload(sys)  
sys.setdefaultencoding('utf8')



dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

LIMITE_DB = 55
THRESHOLD = 2000
CHUNK_SIZE = 1024
FORMAT = pyaudio.paInt16
RATE = 16000

otra = True

nombre_csv=""


nombre_carpeta="rec_"+str(int(time.time()))


lista_ts=[]
lista_sentiment=[]
lista_eDetectadas=[]
lista_eEsperadas=[]
lista_text=[]
lista_trad=[]
lista_respuestas=[]
iteraciones=0
fase=0


fx = (
    AudioEffectsChain()
    .phaser()

    .speed(0.85)
    .reverb()

)


def is_silent(data):
    rms = audioop.rms(data,2) #media cuadratica
    decibel = 20 * math.log10(rms)
    #print(decibel)
    return decibel < LIMITE_DB

def normalize(snd_data):
    "Average the volume out"
    MAXIMUM = 16384
    times = float(MAXIMUM)/max(abs(i) for i in snd_data)

    r = array('h')
    for i in snd_data:
        r.append(int(i*times))
    return r

def add_silence(snd_data, seconds):
    "Add silence to the start and end of 'snd_data' of length 'seconds' (float)"
    r = array('h', [0 for i in xrange(int(seconds*RATE))])
    r.extend(snd_data)
    r.extend([0 for i in xrange(int(seconds*RATE))])
    return r



def grabar():
   
    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT, channels=1, rate=RATE,
        input=True, output=True,
        frames_per_buffer=CHUNK_SIZE)

    num_silent = 0
    snd_started = False

    r = array('h')

    while 1:
        # little endian, signed short
        snd_data = array('h', stream.read(CHUNK_SIZE))
        if byteorder == 'big':
            snd_data.byteswap()

        silent = is_silent(snd_data)

        if silent and snd_started:
            r.extend(snd_data)
            num_silent += 1
        elif not silent and not snd_started:
            r.extend(snd_data)
            snd_started = True
        elif not silent and snd_started:
            r.extend(snd_data)
            num_silent=0
        if snd_started and num_silent > 25:#tiempo de espera de silencio
            break

    sample_width = p.get_sample_size(pyaudio.paInt16)
    stream.stop_stream()
    stream.close()
    p.terminate()

    r = normalize(r)

    r = add_silence(r, 0.5)
    return sample_width, r

def grabar_voz(path):
    sample_width, data = grabar()
    data = pack('<' + ('h'*len(data)), *data)

    wf = wave.open(path, 'wb')
    wf.setnchannels(1)
    wf.setsampwidth(sample_width)
    wf.setframerate(RATE)
    wf.writeframes(data)
    wf.close()

def captura_ruido(num_samples=50):
    """ Obtiene la media de ruido en decibelios, tomando en cuenta el 20% de las muestras
    con mayor valor.
    """

    print("Grabando ruido ambiental...")
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=1,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK_SIZE)

    values = [20 * math.log10(audioop.rms(stream.read(CHUNK_SIZE), 2))
              for x in range(num_samples)] 
    values = sorted(values, reverse=True)
    ruido = sum(values[:int(num_samples * 0.2)]) / int(num_samples * 0.2)
    print ("Media de ruido ambiental: ", ruido)
    stream.close()
    p.terminate()
    
    return ruido    

def reconocer_voz(filename):

    with open(join(dirname(__file__), filename),'rb') as audio_file:
            results = speech_to_text.recognize(
                audio=audio_file, content_type='audio/wav', timestamps=True,
                word_confidence=True, model='es-ES_BroadbandModel')
        
    #print(json.dumps(results,indent=2,ensure_ascii=False).encode('utf8')) #resultados tecnicos
    if results['results']:
        text = results['results'][0]['alternatives'][0]['transcript']
    else:
        text = ""
    return text

def reconocer_voz_v2(filename):
    with open(join(dirname(__file__), filename),'rb') as audio_file:
            results = speech_to_text.recognize(
                content_type='multipart/form-data', timestamps=True,
                word_confidence=True, model='es-ES_BroadbandModel')
        
    #print(json.dumps(results,indent=2,ensure_ascii=False).encode('utf8')) #resultados tecnicos
    if results['results']:
        text = results['results'][0]['alternatives'][0]['transcript']
    else:
        text = ""
    return text

def reproducir_voz(respuesta):
    with open(join(dirname(__file__), 'respuestas/output.wav'),'wb') as audio_file:
            response=text_to_speech.synthesize(respuesta, accept='audio/wav', voice="es-ES_EnriqueVoice")
            audio_file.write(response.content)

    infile='respuestas/output.wav'
    outfile='respuestas/proc_output.wav'

    fx(infile, outfile)

    wf = wave.open('respuestas/proc_output.wav', 'rb')
    p = pyaudio.PyAudio()

    stream = p.open(
        format = p.get_format_from_width(wf.getsampwidth()),
        channels = wf.getnchannels(),
        rate = wf.getframerate(),
        output = True)
    data = wf.readframes(CHUNK_SIZE)

    while data != '':
        stream.write(data)
        data = wf.readframes(CHUNK_SIZE)

    stream.close()
    p.terminate()

def emocion_voz(text):
    #traduccion del texto a ingles para enviar a Tone Analyzer
    traduccion_json = language_translator.translate(text, source='es',target='en')
    traduccion = traduccion_json['translations'][0]['translation']
    
    if traduccion == None:
        traduccion = ""
    #print(traduccion)
    emocion = None

    emocion_json = tone_analyzer.tone(tone_input=traduccion, content_type="text/plain")
    if emocion_json['document_tone']['tones']:
        emociones = emocion_json['document_tone']['tones']

        '''for emocion in emociones:
            #seleccion de emocion
            print(emocion)'''

        emocion = emociones[0]['tone_id']
        if emocion != "joy" and emocion !="anger" and emocion != "sadness" and emocion != "fear":
            emocion = "none"

    return emocion, traduccion
     
def sentimiento_voz(text):
    response = natural_language_understanding.analyze(
            text=text, features=Features(sentiment=SentimentOptions()), language='es')

    #print(json.dumps(response['sentiment'], indent=2))
    sentimiento=response['sentiment']['document']['label']
    valor_sent=response['sentiment']['document']['score']
    return sentimiento, valor_sent

def cambiar_emocion(context):
    emocion_ahora=context['emocionAhora']
    #print(emocion_ahora)

    if emocion_ahora == 'joy':
        context['emocionAhora'] = 'anger'
    elif emocion_ahora == 'anger':
        context['emocionAhora'] = 'joy'
    elif emocion_ahora == 'sadness':
        context['emocionAhora'] = 'fear'
    elif emocion_ahora == 'fear':
        context['emocionAhora'] = 'sadness'

    return context

def cambiar_fase(context):
    emocion_ahora=context['emocionAhora']
    #print(emocion_ahora)

    if emocion_ahora == 'joy':
        context['emocionAhora'] = 'sadness'
    elif emocion_ahora == 'anger':
        context['emocionAhora'] = 'fear'
    elif emocion_ahora == 'sadness':
        context['emocionAhora'] = 'anger'
    elif emocion_ahora == 'fear':
        context['emocionAhora'] = 'joy'


    return context

def guardar_respuestas(context):
    context_aux=deepcopy(context)
    global otra


    if otra == True:
        context_aux=cambiar_emocion(context_aux) #cambiar emocion para guardarla, ya que en el dialogo ha cambiado
        otra = False
    else:
        otra = True

    


    del context_aux['system']
    with open('resultados/json/results_'+context_aux['nombre']+'_'+ str(time.time())+'.json', 'w') as outfile:
        json.dump(context_aux, outfile)
            
def guardar_csv(context, text):

    global iteraciones,lista_ts,lista_text,lista_trad,lista_sentiment,lista_eDetectadas, lista_respuestas, nombre_csv, fase
    context_aux=deepcopy(context)

    linea=""

    datos = []

    alternancia_persona=['Yo','Yo','Tu','Tu','El','El']
    alternancia_emocion=[True,True,False,False,True,False]
    if context_aux['fase2']==True and otra == False:
        context_aux=cambiar_fase(context_aux)

    if otra == True:
        context_aux=cambiar_emocion(context_aux)

    emocion_ahora=context_aux['emocionAhora']

    if emocion_ahora == 'joy':
        emocion_otra = 'anger'
    elif emocion_ahora == 'anger':
        emocion_otra = 'joy'
    elif emocion_ahora == 'sadness':
        emocion_otra = 'fear'
    elif emocion_ahora == 'fear':
        emocion_otra = 'sadness'

    #directo=True

    if fase == 0:
        datos.append("Iteracion, Sujeto, Tipo, Emocion esperada, Emocion detectada, Transcripcion, Traduccion, Respuesta, Observaciones, Latencia")

    for i in range(0, iteraciones):
        linea=linea + str((i+1)+(fase*6))+", "
        linea=linea + str(alternancia_persona[i])+", "

        if i%2 == 0:
            linea=linea+"D, "
        else:
            linea=linea+"I, "
        
        if alternancia_emocion[i] == True:
            linea = linea +emocion_ahora +","
        else:
            linea = linea +emocion_otra +","

        linea = linea + str(lista_eDetectadas[i])+", "
        linea = linea +lista_text[i].encode('utf-8').replace(',', '')+", "
        linea = linea +lista_trad[i].encode('utf-8').replace(',', '')+", "
        linea = linea +lista_respuestas[i].replace(',', '')+", "

        linea = linea + ","
        linea=linea + str(lista_ts[i])+", "

        datos.append(linea)
        linea=""

    if (fase == 3):
        datos.append("Carpeta grabaciones: ,"+nombre_carpeta)

    if os.path.exists(nombre_csv):
        filecsv = open(nombre_csv, 'a')
    else:
        nombre_csv='resultados/csv/results_'+context_aux['nombre']+'_'+ str(int(time.time()))+'.csv'
        filecsv = open(nombre_csv, 'w')

    for line in datos:
        filecsv.write("%s\n" % line)

    filecsv.close()

    lista_text=[]
    lista_trad=[]
    lista_sentiment=[]
    lista_ts=[]
    lista_eDetectadas=[]
    lista_respuestas=[]
    iteraciones=0
    fase=fase+1


def respuesta_chat(response,text,emocion):
    context = response['context'] #contexto para continuar conversacion

    #print(json.dumps(context, indent=2))


    if context['guardar'] == True:
        print("Guardando resultados...")
        guardar_csv(context, text)
        guardar_respuestas(context)
        context['guardar'] = False
        #segunda vuelta, guardar contexto de la primera
        
    if emocion:
        context['emocion'] = emocion
    else:
        context['emocion'] = "none"


    response = conversation.message(workspace_id=workspace_id, input={'text': text},context=context)

    return response

def welcome():
    response_welcome = conversation.message(
        workspace_id = workspace_id,
            input = {
            'text': ''
        }
    )
    if response_welcome['output']['text']:
        welcome=response_welcome['output']['text'][0]
        print(welcome)
        reproducir_voz(welcome)
    return response_welcome


def iniciar_servicios():
    username_stt = os.environ.get("BLUEMIX_USERNAME")
    password_stt = os.environ.get("BLUEMIX_PASSWORD")
    username_con = os.environ.get("CONVERSATION_USERNAME")
    password_con = os.environ.get("CONVERSATION_PASSWORD")
    username_tts = os.environ.get("TTS_USERNAME")
    password_tts = os.environ.get("TTS_PASSWORD")

    username_tone = os.environ.get("TONE_USERNAME")
    password_tone = os.environ.get("TONE_PASSWORD")
    username_trans = os.environ.get("TRANS_USERNAME")
    password_trans = os.environ.get("TRANS_PASSWORD")

    username_nlu = os.environ.get("NLU_USERNAME")
    password_nlu = os.environ.get("NLU_PASSWORD")

    username_disc = os.environ.get("DISCOVERY_USERNAME")
    password_disc = os.environ.get("DISCOVERY_PASSWORD")   

    speech_to_text = SpeechToTextV1(
        username=username_stt, 
        password=password_stt,
    )

    conversation = ConversationV1(
        username=username_con,
        password=password_con,
        version='2018-02-16'
    )

    text_to_speech = TextToSpeechV1(
        username=username_tts,
        password=password_tts,
    )  

    workspace_id = '329ee5f3-fd20-47c6-8cd8-e2a78a245fe3'
    if os.getenv("conversation_workspace_id") is not None:
        workspace_id = os.getenv("conversation_workspace_id")

    tone_analyzer = ToneAnalyzerV3(
        username=username_tone,
        password=password_tone,
        version='2017-09-26'
    )

    language_translator = LanguageTranslatorV2(
        username=username_trans,
        password=password_trans
    )

    natural_language_understanding = NaturalLanguageUnderstandingV1(
        version='2017-02-27',
        username=username_nlu,
        password=password_nlu
    )

    discovery = DiscoveryV1(
        version='2017-10-16',
        username=username_disc,
        password=password_disc
    )
    return speech_to_text, conversation, text_to_speech, workspace_id, tone_analyzer, language_translator, natural_language_understanding, discovery


if __name__ == '__main__':

    speech_to_text, conversation, text_to_speech, workspace_id, tone_analyzer, language_translator, natural_language_understanding, discovery = iniciar_servicios()

    ruido = captura_ruido()

   
    os.makedirs('grabaciones/'+nombre_carpeta)

    if ruido > LIMITE_DB:
        LIMITE_DB=ruido

    response = welcome()

    while 1:

        start_total=(time.time())*1000.0

        print("\n")
        print("Escuchando...")
        nombre_fichero='rec_'+str(iteraciones+(fase*6))+'_'+str(int(time.time()))+'.waw'
        filename = 'grabaciones/'+nombre_carpeta+'/'+nombre_fichero
        grabar_voz(filename)
        print("Fin de escucha.")


        start_rec=(time.time())*1000.0

        text = reconocer_voz(filename)

        #text = 'Hola, estoy contento y un poco enfadado, pero también triste'

        end_rec=(time.time())*1000.0
        latencia_s2t=end_rec - start_rec
        
        print(text)

        if text:

            start_rec=(time.time())*1000.0

            emocion, traduccion = emocion_voz(text)

            end_rec=(time.time())*1000.0
            latencia_tone=end_rec - start_rec

            print(emocion)

            start_rec=(time.time())*1000.0

            sentimiento, valor_sent = sentimiento_voz(text)

            end_rec=(time.time())*1000.0
            latencia_nlu=end_rec - start_rec

            print(sentimiento+' = '+str(valor_sent))
            

            start_rec=(time.time())*1000.0

            response = respuesta_chat(response, text, emocion)

            end_rec=(time.time())*1000.0
            latencia_chat=end_rec - start_rec
            
            #print(json.dumps(response, indent=2)) #resultados tecnicos


            print(colored("\n/------/-------/--RESPUESTA--/--------/--------/\n",'yellow'))

            respuestas_guardar=""


            for respuestas in response['output']['text']:
                if respuestas != "":
                    print(respuestas)
                    reproducir_voz(respuestas)
                    respuestas.encode('utf-8')

                    respuestas_guardar=respuestas_guardar+respuestas+". "

                    start_rec=(time.time())*1000.0
                    
                    end_rec=(time.time())*1000.0
                    latencia_t2s=end_rec - start_rec
            print(colored("\n/-------/---------/---------/--------/---------/\n",'yellow'))

            '''if len(response['output']['text']) > 1 and response['output']['text'][1] != "":
                respuesta2=response['output']['text'][1]
                print(respuesta2)
                reproducir_voz(respuesta2)'''

            end_total=(time.time())*1000.0
            latencia_total=latencia_s2t + latencia_tone + latencia_nlu + latencia_chat + latencia_t2s

            #guardar latencia total y sentiment en arrays para luego grabarlos en json y csv

            if response['context']['noGuardar'] == True:
                response['context']['noGuardar'] = False
                os.remove('grabaciones/'+nombre_carpeta+'/'+nombre_fichero)
            else:
                lista_ts.append(latencia_total)
                lista_sentiment.append(valor_sent)
                lista_eDetectadas.append(emocion)
                lista_text.append(text)
                lista_trad.append(traduccion)
                lista_respuestas.append(respuestas_guardar)
                iteraciones += 1


            '''
            print("-----------------------------------------")
            print('ts s2t: '+str(latencia_s2t))
            print('ts tone: '+str(latencia_tone))
            print('ts nlu: '+str(latencia_nlu))
            print('ts chat: '+str(latencia_chat))
            print('ts t2s: '+str(latencia_t2s))
            print('_-- ts TOTAL: '+str(latencia_total)+'--_')
            print("-----------------------------------------")

            '''

        else:
            text_repite="No te he escuchado bién, repitelo por favor"
            print(text_repite)
            reproducir_voz(text_repite)

            


       












