# README #

Este es el repositorio que contiene los archivos fuentes y recursos necesarios para el TFG y sus pruebas.



### Contenido ###

* TFG-Audios: Carpeta con proyectos de audacity y archivos de audio pertenecientes a las pruebas de reconocimiento
* TFG-Google: Prueba de reconocimiento con Google Speech API
* TFG-Watson: Prueba de reconocimiento de audio con IBM Watson S2T
* TFG-Bing: Prueba de reconocimiento con Bing
* TFG-AIML: Prueba de bot con AIML 2.0, desarrollado en Playground PandoraBots


